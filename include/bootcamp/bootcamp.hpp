// Copyright 2021 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.

/// \copyright Copyright 2021 The Autoware Foundation
/// \file
/// \brief This file defines the bootcamp class.

#ifndef BOOTCAMP__BOOTCAMP_HPP_
#define BOOTCAMP__BOOTCAMP_HPP_

#include <bootcamp/visibility_control.hpp>

#include <cstdint>

namespace autoware
{
/// \brief TODO(shumin): Document namespaces!
namespace bootcamp
{

/// \brief TODO(shumin): Document your functions
int32_t BOOTCAMP_PUBLIC print_hello();

}  // namespace bootcamp
}  // namespace autoware

#endif  // BOOTCAMP__BOOTCAMP_HPP_
